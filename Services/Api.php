<?php namespace C4\ApiBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use GuzzleHttp\Exception\RequestException;

class Api
{
    protected $container;
    public $client = null;
    public $google = null;
    private $session;

    public $token = null;
    public $token_expires_at = null;

    public function __construct(Container $container)
    {
        $this->container = $container;
//        $this->client = $this->container->get('guzzle.client.teamwork_'.$this->container->getParameter("env"));
        $this->client = $this->container->get('guzzle.client.default');
        $this->google = $this->container->get('guzzle.client.google');
        $this->session = new Session();
        $this->token = $this->session->get("api_token");
        $this->token_expires_at = $this->session->get("api_token_expires_at");
    }

    public function token($token = null)
    {
        if ($token) {
            $this->session->set("api_token", $token);
            $this->token = $token;
        }

        return $this->token;
    }

    public function token_expires_at($date = null)
    {
        if ($date) {
            $this->session->set("api_token_expires_at", $date);
            $this->token_expires_at = $date;
        }

        return $this->token_expires_at;
    }

    public function mb_adminLogin($data)
    {
        /*
         * $data:
         * username
         * password
         */
        try {
            $response = $this->client->post("/mb/admin-login", ['form_params' => $data,]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_banklinkLogin($data)
    {
        /*
        * $data:
        * pid
        * email
        * auth_provider
        * auth_action_id
        */
        try {
            $response = $this->client->post("/mb/banklink-login", ['form_params' => $data,]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_checkProfile($data)
    {
        /*
         * pid
         * auth_provider
         * auth_action_id
         */
        try {
            $response = $this->client->post("/mb/check_profile", ['form_params' => $data,]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_checkService()
    {
        try {
            $response = $this->client->post("/mb/check-service");
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_checkSession()
    {
        /*
         * pid
         * auth_provider
         * auth_action_id
         */
        try {
            $response = $this->client->post(
                "/mb/check-session",
                [
                    'headers' => ["X-API-SESSION" => $this->token,],
//                    'form_params' => [
//                        "token" => $this->token,
//                    ],
                ]
            );
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_checkToken($token)
    {
        try {
            $response = $this->client->post(
                "/mb/check-token",
                [
//                    'headers' => ["X-API-SESSION" => $this->token,],
                    'form_params' => [
                        "token" => $token,
                    ],
                ]
            );
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_checkUsername($data)
    {
        /*
         * username
         */
        try {
            $response = $this->client->post("/mb/check-username", ['form_params' => $data,]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_getInvoice($data)
    {
        /*
         * invoice_id
         */
        try {
            $response = $this->client->post("/mb/get-invoice", ['headers' => ["X-API-SESSION" => $this->token,], 'form_params' => $data,]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = $response->getBody();
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_invoicesList()
    {
        try {
            $response = $this->client->post("/mb/invoices-list", ['headers' => ["X-API-SESSION" => $this->token,],]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_login($data)
    {
        /*
         * email
         * password
         */
        try {
            $response = $this->client->post("/mb/login", ['form_params' => $data,]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_passwordResetConfirm($data)
    {
        /*
         * token
         * password
         */
        try {
            $response = $this->client->post("/mb/password-reset-confirm", ['form_params' => $data,]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_passwordReset($data)
    {
        /*
         * email
         * confirmation_url
         *
            {
              "email": "user01@test.bizz",
              "confirmation_url": "https://mansbaltcom.lv/reset-password/?token=__token__"
            }
         */
        try {
            $response = $this->client->post("/mb/password-reset", ['form_params' => $data,]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_powerObjects()
    {
        try {
            $response = $this->client->post("/mb/power-objects", ['headers' => ["X-API-SESSION" => $this->token,],]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_signUp($data)
    {
        try {
            $response = $this->client->post("/mb/sign-up", ['form_params' => $data,]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_signUpConfirm($data)
    {
        /*
         * token
         */
        try {
            $response = $this->client->post("/mb/sign-up-confirm", ['form_params' => $data,]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    public function mb_userData()
    {
        try {
            $response = $this->client->post("/mb/user-data", ['headers' => ["X-API-SESSION" => $this->token,],]);
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();
            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }

    // GOOGLE
    public function googleRecaptcha($data = [])
    {
        try {
            $response = $this->google->post("/recaptcha/api/siteverify", ["form_params" => $data]);

            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        } catch (RequestException $e) {
            $response = $e->getResponse();

            if (!$response) {
                $status = 0;
                $body = "timeout";
            } else {
                $status = $response->getStatusCode();
                $body = json_decode($response->getBody(), true);
            }
        }

        return ["status" => $status, "body" => $body];
    }
}