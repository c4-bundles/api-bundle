<?php namespace C4\ApiBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;

class GlobalsExtension extends \Twig_Extension
{
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function getGlobals() {
        return array(
            'api' => $this->container->get('api'),
        );
    }

    public function getName() {
        return 'ApiBundle:GlobalsExtension';
    }
}